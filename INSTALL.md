Installing Mouseover Detect
=================================

The mouseover_detect module manages its dependencies via composer. So if 
you simply downloaded this module from drupal.org you have to delete it and 
install it again via composer!

Simply change into Drupal directory and use composer to install 
mouseover_detect plugin and drupal module:

```
cd $DRUPAL
composer require cristian100/mouseover_detect
composer require drupal/mouseover_detect
```

Once these has been installed, enable your module and enjoy.
